package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// ParseVars parses the given yaml file into a map of interfaces.
func ParseVars(conf string) (map[interface{}]interface{}, error) {
	data, err := ioutil.ReadFile(conf)
	if err != nil {
		return nil, err
	}

	config := make(map[interface{}]interface{})
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
