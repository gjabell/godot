package main

import (
	"io/ioutil"
	"text/template"
)

// ParseTemplate parses the given file into a Template.
func ParseTemplate(src string) (*template.Template, error) {
	data, err := ioutil.ReadFile(src)
	if err != nil {
		return nil, err
	}

	t, err := template.New(src).Parse(string(data))
	if err != nil {
		return nil, err
	}

	return t, nil
}
