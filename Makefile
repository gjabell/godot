PROJECT_NAME := "godot"
PKG := "gitlab.com/gjabell/$(PROJECT_NAME)"

.PHONY: all dep build clean test coverage report lint

all: build

lint:
	@golint -set_exit_status $(PKG)

test:
	@go test ./...

race:
	@go test -race ./...

msan:
	@(export CC=clang-6.0 && go test -msan ./...)

coverage:
	@(mkdir -p cover && go test -coverprofile "cover/coverage.cov" "$(PKG)")

report: coverage
	@go tool cover -html=cover/coverage.cov -o cover/coverage.html

build:
	@go build -i -v $(PKG)

clean:
	@rm -rf $(PROJECT_NAME) cover/
