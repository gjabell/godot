package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type MakerTestSuite struct {
	suite.Suite
	Buf     bytes.Buffer
	TempDir string
	Err     error
}

func (s *MakerTestSuite) SetupTest() {
	// set stdout to buffer
	log.SetOutput(&s.Buf)
	s.TempDir, s.Err = ioutil.TempDir("", "test")
	if s.Err != nil {
		s.T().Fatalf("failed to create temp dir: %s", s.Err)
	}
}

func (s *MakerTestSuite) TearDownTest() {
	log.SetOutput(os.Stderr)
	if s.Err = os.RemoveAll(s.TempDir); s.Err != nil {
		log.Printf("failed to remove temp dir: %s", s.Err)
	}
}

func TestMakerTestSuite(t *testing.T) {
	suite.Run(t, new(MakerTestSuite))
}

// MakeDir Tests

func (s *MakerTestSuite) TestMakeDirDry() {
	c := &Context{Dry: true}

	s.Err = c.MakeDir("out")

	assert.Nil(s.T(), s.Err, "error should be nil")
}

func (s *MakerTestSuite) TestMakeDirVerbose() {
	c := &Context{Dry: true, Verbose: true}

	s.Err = c.MakeDir("out")

	assert.Nil(s.T(), s.Err, "error should be nil")
	assert.Contains(s.T(), s.Buf.String(), "(d) out", "log output should contain message")
}

func (s *MakerTestSuite) TestMakeDirSuccess() {
	c := &Context{}
	p := path.Join(s.TempDir, "dir")

	s.Err = c.MakeDir(p)

	assert.Nil(s.T(), s.Err, "error should be nil")

	var info os.FileInfo
	info, s.Err = os.Stat(p)

	assert.Nil(s.T(), s.Err, "error should be nil")
	assert.True(s.T(), info.IsDir(), "directory should be present")
}

// Make Template Tests

func (s *MakerTestSuite) TestMakeTemplateDry() {
	c := &Context{Dry: true}

	s.Err = c.MakeTemplate("in", "out")

	assert.Nil(s.T(), s.Err, "error should be nil")
}

func (s *MakerTestSuite) TestMakeTemplateVerbose() {
	c := &Context{Dry: true, Verbose: true}

	s.Err = c.MakeTemplate("in", "out")

	assert.Nil(s.T(), s.Err, "error should be nil")
	assert.Contains(s.T(), s.Buf.String(), "(f) in -> out", "log output should contain message")
}

func (s *MakerTestSuite) TestMakeTemplateBinary() {
	c := &Context{Src: tmplDir, Dst: s.TempDir}
	src, dst := tmplDir+"/binary", s.TempDir+"/binary"

	in, _ := ioutil.ReadFile(src)

	s.Err = c.MakeTemplate(src, dst)

	assert.Nil(s.T(), s.Err, "error should be nil")

	out, _ := ioutil.ReadFile(dst)

	assert.FileExists(s.T(), dst, "binary file should exist")
	assert.Equal(s.T(), in, out, "binary files should have same contents")
}

func (s *MakerTestSuite) TestMakeTemplateSimple() {
	c := &Context{Src: tmplDir, Dst: s.TempDir, Vars: map[interface{}]interface{}{"Name": "simple"}}
	src, dst := tmplDir+"/simple", s.TempDir+"/simple"

	in, _ := ioutil.ReadFile(src)

	s.Err = c.MakeTemplate(src, dst)

	assert.Nil(s.T(), s.Err, "error should be nil")

	out, _ := ioutil.ReadFile(dst)

	assert.FileExists(s.T(), dst, "simple file should exist")
	assert.NotEqual(s.T(), in, out, "template and output should have different contents")
	assert.Equal(s.T(), "Here's a simple test.\n", string(out), "simple output should have executed template contents")
}
