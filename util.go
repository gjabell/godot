package main

import (
	"io"
	"io/ioutil"
	"os"
	"unicode/utf8"
)

// IsUtf8 checks if the given file is utf8 encoded.
// TODO there might be a better way to solve this than reading the whole file.
func IsUtf8(file string) bool {
	bytes, err := ioutil.ReadFile(file)

	return err == nil && utf8.Valid(bytes)
}

// CopyFile copies the file in the input path to the output path.
func CopyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	return
}

// StringInList checks if the given list contains the given string.
func StringInList(str string, lst *[]string) bool {
	for _, item := range *lst {
		if str == item {
			return true
		}
	}

	return false
}
