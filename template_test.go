package main

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var tmplDir = "testdata/template"

func TestParseTemplateSimple(t *testing.T) {
	d := struct{ Name string }{Name: "simple"}

	template, err := ParseTemplate(tmplDir + "/simple")
	if err != nil {
		t.Fatalf("Failed to parse template file: %s", err)
	}

	assert.Equal(t, tmplDir+"/simple", template.Name(), "name should be filename")

	var w bytes.Buffer
	err = template.Execute(&w, &d)
	if err != nil {
		t.Fatalf("Failed to execute template: %s", err)
	}

	assert.Equal(t, "Here's a simple test.", strings.TrimSpace(w.String()), "output should be executed template")
}

func TestParseTemplateNested(t *testing.T) {
	d := struct {
		Name string
		Nest struct{ Value string }
	}{
		"nested",
		struct {
			Value string
		}{
			"nested",
		},
	}

	template, err := ParseTemplate(tmplDir + "/nested")
	if err != nil {
		t.Fatalf("Failed to parse template file: %s", err)
	}

	assert.Equal(t, tmplDir+"/nested", template.Name(), "name should be filename")

	var w bytes.Buffer
	err = template.Execute(&w, &d)
	if err != nil {
		t.Fatalf("Failed to execute template: %s", err)
	}

	assert.Equal(t, "Here's a nested template. The nest value is nested.", strings.TrimSpace(w.String()), "output should be executed template")
}

func TestParseTemplateBool(t *testing.T) {
	d := struct {
		Name string
		Show struct {
			Show  bool
			Value string
		}
		Hide struct {
			Show  bool
			Value string
		}
	}{
		"bool",
		struct {
			Show  bool
			Value string
		}{true, "visible"},
		struct {
			Show  bool
			Value string
		}{false, "invisible"},
	}

	template, err := ParseTemplate(tmplDir + "/bool")
	if err != nil {
		t.Fatalf("Failed to parse template file: %s", err)
	}

	assert.Equal(t, tmplDir+"/bool", template.Name(), "name should be filename")

	var w bytes.Buffer
	err = template.Execute(&w, &d)
	if err != nil {
		t.Fatalf("Failed to execute template: %s", err)
	}

	assert.Equal(t, "The bool template checks visibility. This should be visible.", strings.TrimSpace(w.String()), "output should be executed template")
}
