package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsUtf8Binary(t *testing.T) {
	path := tmplDir + "/binary"

	assert.False(t, IsUtf8(path), "Binary file should not be UTF-8")
}

func TestIsUtf8NonBinary(t *testing.T) {
	path := tmplDir + "/bool"

	assert.True(t, IsUtf8(path), "Non-binary should be UTF-8")
}

func TestStringInList(t *testing.T) {
	l := []string{"this", "is", "a", "test"}

	assert.True(t, StringInList("this", &l), "String 'this' should be in list")
	assert.True(t, StringInList("is", &l), "String 'is' should be in list")
	assert.True(t, StringInList("test", &l), "String 'test' should be in list")
	assert.False(t, StringInList("not", &l), "String 'not' shouldn't be in list")
}
