# GoDot - A Command-line Dotfile Templater

[![pipeline status](https://gitlab.com/gjabell/godot/badges/master/pipeline.svg)](https://gitlab.com/gjabell/godot/commits/master) [![coverage report](https://gitlab.com/gjabell/godot/badges/master/coverage.svg)](https://gitlab.com/gjabell/godot/commits/master) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/gjabell/godot/raw/master/LICENSE)

## Introduction

'Dotfiles' are configuration files typically found on a Unix system (like Linux or MacOS). On a Unix system, files
starting with a period (or dot) are hidden files often used for configuration, hence the name. Many people keep their
dotfiles in a single git repository to make it easy to back up and track changes, and use a tool such as [GNU Stow](https://www.gnu.org/software/stow/)
to link them to the proper directories.

GoDot is a command-line tool designed to make it easy to maintain multiple variations of your dotfiles without having to
manually edit them or keep multiple versions of the same (probably similar files). This tool might be useful if you:

- change color themes a lot and don't want to manually change all of your files every time you switch themes, or
- use multiple computers and want to have a mostly-similar-but-slightly-different setup on each computer.

GoDot functions by recursively traversing a directory containing template files, parsing each template and writing out
the executed template into a specified output directory. As such, it is best used with the aforementioned GNU Stow (or
a similar program) to move the executed templates to the correct configuration directories.

## Installation

GoDot can be installed using the Go toolchain: `go get -u gitlab.com/gjabell/godot`. This will install GoDot to `$GOPATH/bin/godot`.
For instructions on installing the Go toolchain, please visit Go's [Getting Started](https://golang.org/doc/install) page.

## Usage

GoDot reads variables from YAML files, and uses them to execute Go text templates. The YAML documentation can be found [here](http://yaml.org/),
while the Go template documentation can be found [here](https://golang.org/pkg/text/template/). Please note that Go's template
system is case sensitive, so if you list variables in your YAML files as lowercase, they should also be lowercase in your templates.

GoDot supports the following command line arguments:
```bash
-h  --help     Print help information
-i  --in       Template file or directory
-o  --out      Output file or directory
-v  --vars     List of variable files. Files will be loaded sequentially and
               merged into a single context, so duplicate entries will use
               the last entry value. Default: [vars.yml]
-s  --skips    List of directories or files to ignore. Default: [.git .gitignore]
-d  --dry      Don't generate files (best used with verbose flag).
-b  --verbose  Print verbose output to console.
```

A typical usage might look like: `godot -i templates -o dotfiles -v colors.yml`

## Example

The following is an example .Xresources file based on the Dracula color scheme:

`colors.yaml`:
```yaml
colors:
  fg:      'F8F8F2'
  bg:      '282A36'
  color0:  '000000'
  color8:  '4D4D4D'
  color1:  'FF5555'
  color9:  'FF6E67'
  color2:  '50FA7B'
  color10: '5AF78E'
  color3:  'F1FA8C'
  color11: 'F4F99D'
  color4:  'BD93F9'
  color12: 'CAA9FA'
  color5:  'FF79C6'
  color13: 'FF92D0'
  color6:  '8BE9FD'
  color14: '9AEDFE'
  color7:  'BFBFBF'
  color15: 'E6E6E6'
```

`template`:
```
*.foreground: #{{ .colors.fg }}
*.background: #{{ .colors.bg }}
*.color0:     #{{ .colors.color0 }}
*.color8:     #{{ .colors.color8 }}
*.color1:     #{{ .colors.color1 }}
*.color9:     #{{ .colors.color9 }}
*.color2:     #{{ .colors.color2 }}
*.color10:    #{{ .colors.color10 }}
*.color3:     #{{ .colors.color3 }}
*.color11:    #{{ .colors.color11 }}
*.color4:     #{{ .colors.color4 }}
*.color12:    #{{ .colors.color12 }}
*.color5:     #{{ .colors.color5 }}
*.color13:    #{{ .colors.color13 }}
*.color6:     #{{ .colors.color6 }}
*.color14:    #{{ .colors.color14 }}
*.color7:     #{{ .colors.color7 }}
*.color15:    #{{ .colors.color15 }}
```

Running the command `godot -i template -o .Xresources -v colors.yaml` would produce the following file:

`.Xresources`
```
*.foreground: #F8F8F2
*.background: #282A36
*.color0:     #000000
*.color8:     #4D4D4D
*.color1:     #FF5555
*.color9:     #FF6E67
*.color2:     #50FA7B
*.color10:    #5AF78E
*.color3:     #F1FA8C
*.color11:    #F4F99D
*.color4:     #BD93F9
*.color12:    #CAA9FA
*.color5:     #FF79C6
*.color13:    #FF92D0
*.color6:     #8BE9FD
*.color14:    #9AEDFE
*.color7:     #BFBFBF
*.color15:    #E6E6E6
```

## Contributing

This tool is still in development, so there will probably be things that don't work. I am mainly developing it for my own
use, but I would like for it to be useful to others as well! If you find bugs or would like a new feature, please feel free
to open an issue or make a pull request!

## Version History

- [1.0.0](https://gitlab.com/gjabell/godot/tags/v1.0.0) - Initial release

## License

MIT License

Copyright (c) 2018 Galen Abell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
