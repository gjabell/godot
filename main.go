package main

import (
	"fmt"
	"log"
	"os"

	"github.com/akamensky/argparse"
)

func main() {
	parser := argparse.NewParser("godot", "Command-line dotfile templater")

	in := parser.String("i", "in", &argparse.Options{Required: true, Help: "Template file or directory"})
	out := parser.String("o", "out", &argparse.Options{Required: true, Help: "Output file or directory"})
	varList := parser.List("v", "vars", &argparse.Options{
		Required: false,
		Help:     "List of variable files. Files will be loaded sequentially and merged into a single context, so duplicate entries will use the last entry value",
		Default:  []string{"vars.yml"}})
	skips := parser.List("s", "skips", &argparse.Options{
		Required: false,
		Help:     "List of directories or files to ignore",
		Default:  []string{".git", ".gitignore"}})
	dry := parser.Flag("d", "dry", &argparse.Options{Required: false, Help: "Don't generate files (best used with verbose flag)."})
	verbose := parser.Flag("b", "verbose", &argparse.Options{Required: false, Help: "Print verbose output to console."})

	if err := parser.Parse(os.Args); err != nil {
		fmt.Print(parser.Usage(err))
		return
	}

	// Parse variables
	vars := make(map[interface{}]interface{})
	for _, v := range *varList {
		current, err := ParseVars(v)
		if err != nil {
			log.Fatalf("Failed to parse %s var file: %s", v, err)
		}

		for key, value := range current {
			vars[key] = value
		}
	}

	// Create the context
	ctx := &Context{Src: *in, Dst: *out, Vars: vars, Skips: *skips, Dry: *dry, Verbose: *verbose}

	// Run the program
	if err := ctx.Run(); err != nil {
		log.Fatalf("Error: %s", err)
	}
}
