package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
)

// MakeDir creates the given output directory.
func (c *Context) MakeDir(dst string) error {
	// If verbose, print info.
	if c.Verbose {
		log.Printf("(d) %s", dst)
	}

	// If dry run, don't perform the operation.
	if c.Dry {
		return nil
	}

	return os.MkdirAll(dst, 0744)
}

// MakeTemplate creates the given dst file by parsing or copying the src file.
func (c *Context) MakeTemplate(src, dst string) error {
	// If verbose, print info.
	if c.Verbose {
		log.Printf("(f) %s -> %s", src, dst)
	}

	// If dry run, don't perform the operation.
	if c.Dry {
		return nil
	}

	// If the file isn't a template, copy it instead.
	if !IsUtf8(src) {
		return CopyFile(src, dst)
	}

	// Stat the file to get permissions
	f, err := os.Stat(src)
	if err != nil {
		return err
	}

	// Otherwise we're a template; parse and create a new file.
	t, err := ParseTemplate(src)
	if err != nil {
		return err
	}

	var w bytes.Buffer
	if err = t.Execute(&w, c.Vars); err != nil {
		return err
	}

	return ioutil.WriteFile(dst, w.Bytes(), f.Mode())
}
