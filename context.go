package main

import "os"

// Context represents the program's configuration.
type Context struct {
	Src     string
	Dst     string
	Vars    map[interface{}]interface{}
	Skips   []string
	Dry     bool
	Verbose bool
}

// Run kicks off the whole program.
func (c *Context) Run() error {
	f, err := os.Stat(c.Src)
	if err != nil {
		return err
	}

	if f.IsDir() {
		return c.Walk(c.Src, c.Dst)
	}

	return c.MakeTemplate(c.Src, c.Dst)
}
