package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var confDir = "testdata/config"

func TestParseConfigSimple(t *testing.T) {
	config, err := ParseVars(confDir + "/simple.yaml")
	if err != nil {
		t.Fatalf("Failed to Parse config file: %s", err)
	}

	assert.Equal(t, "simple", config["name"], "name should be 'simple'")
}

func TestParseConfigNested(t *testing.T) {
	config, err := ParseVars(confDir + "/nested.yaml")
	if err != nil {
		t.Fatalf("Failed to parse config file: %s", err)
	}

	name := config["name"]
	nest := config["nest"].(map[interface{}]interface{})

	assert.Equal(t, "nested", name, "name should be 'nested'")
	assert.Equal(t, "nested", nest["value"], "nest -> value should be 'nested'")
}

func TestParseConfigBool(t *testing.T) {
	config, err := ParseVars(confDir + "/bool.yaml")
	if err != nil {
		t.Fatalf("Failed to parse config file: %s", err)
	}

	name := config["name"]
	show := config["show"].(map[interface{}]interface{})
	hide := config["hide"].(map[interface{}]interface{})

	assert.Equal(t, "bool", name, "name should be 'bool'")
	assert.Equal(t, true, show["show"], "show -> show should be true")
	assert.Equal(t, "visible", show["value"], "show -> value should be 'visible'")
	assert.Equal(t, false, hide["show"], "hide -> show should be true")
	assert.Equal(t, "invisible", hide["value"], "hide -> value should be 'invisible'")
}
