package main

import (
	"io/ioutil"
	"log"
	"path"
)

// Walk recursively walks the source directory, creating directories or parsing templates as needed.
func (c *Context) Walk(src, dst string) error {
	nodes, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}

	for _, node := range nodes {
		name := node.Name()
		if StringInList(name, &c.Skips) {
			log.Printf("skipping ignored file or directory: %s", name)
			continue
		}

		newSrc := path.Join(src, name)
		newDst := path.Join(dst, name)

		if node.IsDir() {
			// First create the new directory.
			if err = c.MakeDir(newDst); err != nil {
				return err
			}

			// Then walk it.
			if err = c.Walk(newSrc, newDst); err != nil {
				return err
			}
		} else {
			if err = c.MakeTemplate(newSrc, newDst); err != nil {
				return err
			}
		}
	}

	return nil
}
